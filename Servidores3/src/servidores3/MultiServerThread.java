package servidores3;
import java.net.*;
import java.io.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MultiServerThread extends Thread {
   private Socket socket = null;
 
   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      Servidor1.NoClients++;
   }
   
   public void LeerClima() throws IOException{
   URL climaweb;
   BufferedReader bf;
   File arch;
   BufferedWriter bw;
       try {
            climaweb = new URL("http://www.aire.cdmx.gob.mx/default.php");
            bf = new BufferedReader(new InputStreamReader(climaweb.openStream()));
            arch = new File("descarga.txt");
            bw = new BufferedWriter(new FileWriter(arch));
            String cadena;
            
            
       } catch (MalformedURLException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       }
  
   }
   //public String[] divisiones;
   public void run() {
       String [] divisiones;
      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn;
              
	     while((lineIn = entrada.readLine()) != null){
                 escritor.flush();
                 if (lineIn.length()<2)
                     lineIn = lineIn+"-----";
                 if (lineIn.charAt(0)== '#' && lineIn.charAt(lineIn.length()-1)== '#'){
                     divisiones = lineIn.split("#");
                     String Resp = "";
                     
                     int npal = 0;
                     switch (divisiones[1].toUpperCase()){
                         
                         case "MAY": //servicio 3
                             try{
                                 Resp = "#R:MAY#" + String.valueOf(npal);
                                 for(int x = 0; x<npal; x++){
                                     Resp = "#" + divisiones[3+x].toUpperCase();
                                 }
                                 Resp = Resp + "#";
                                 escritor.println(Resp);
                                 escritor.flush();
                             }catch(Exception e){
                                 System.out.println ("Error : " + e.toString());
                             }
                         break;
                         
                         case "MIN": //servicio 4
                             try{
                                 Resp = "#R:MIN#" + String.valueOf(npal);
                                 for(int x = 0; x<npal; x++){
                                     Resp = "#" + divisiones[3+x].toLowerCase();
                                 }
                                 Resp = Resp + "#";
                                 escritor.println(Resp);
                                 escritor.flush();
                             }catch(Exception e){
                                 System.out.println("Error : " + e.toString());
                             }
                         break;
                         

        
                         default:
                             Resp = "ADVERTENCIA: FORMATO INCOMPLETO O ERRONEO";
                             Resp = Resp + " | Formato:#SERVICE#Numero_datos#datos#...#datos#";
                             escritor.println(Resp);
                             escritor.flush();
                         break;
                     }
                         
                     
                 }else{
                 
                
                System.out.println("Received: "+lineIn);
                escritor.flush();
                if(lineIn.equals("FIN")){
                   Servidor1.NoClients--;
			      break;
                }else{
               escritor.println("Echo... "+lineIn);
               escritor.flush();
            }
             }
             
                     
            }
             
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
} 