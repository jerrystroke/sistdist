/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidores3;
import java.net.*;
import java.io.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Gerardo
 */
public class MultiServerThread3 extends Thread{
    private Socket socket = null;
    
    public MultiServerThread3(Socket socket) {
      super("MultiServerThread3");
      this.socket = socket;
      Servidor2.NoClients++;
   }
    
   public void run() {
       String [] divisiones;
      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn;
              
	     while((lineIn = entrada.readLine()) != null){
                 escritor.flush();
                 if (lineIn.length()<2)
                     lineIn = lineIn+"-----";
                 if (lineIn.charAt(0)== '#' && lineIn.charAt(lineIn.length()-1)== '#'){
                     divisiones = lineIn.split("#");
                     String Resp = "";
                     
                     int npal = 0;
                     switch (divisiones[1].toUpperCase()){
                         case "INV": //servicio 1
                             try{
                             npal = Integer.parseInt(divisiones[2]);
                             String[] derecho = new String[npal];
                             String[] alreves = new String[npal];
                             Resp = "#R:INV#" + String.valueOf(npal);
                             
                             for(int x = 0; x<npal; x++){
                                 derecho[x] = divisiones[3+x];
                                 alreves[x] = "";
                                 for(int y = derecho[x].length()-1; y>=0; y--){
                                     alreves[x] = alreves[x] + derecho[x].charAt(y);
                                 }
                                 Resp = Resp + "#" +alreves[x];
                             }
                             Resp = Resp + "#";
                             escritor.println(Resp);
                             escritor.flush();
                             }catch(Exception e){
                              System.out.println ("Error : " + e.toString()); 
                             }
                         break;
                         
                         
                         case "MIN": //servicio 4
                             try{
                                 Resp = "#R:MIN#" + String.valueOf(npal);
                                 for(int x = 0; x<npal; x++){
                                     Resp = "#" + divisiones[3+x].toLowerCase();
                                 }
                                 Resp = Resp + "#";
                                 escritor.println(Resp);
                                 escritor.flush();
                             }catch(Exception e){
                                 System.out.println("Error : " + e.toString());
                             }
                         break;

                         default:
                             Resp = "ADVERTENCIA: FORMATO INCOMPLETO O ERRONEO";
                             Resp = Resp + " | Formato:#SERVICE#Numero_datos#datos#...#datos#";
                             escritor.println(Resp);
                             escritor.flush();
                         break;
                     }
                         
                     
                 }else{
                 
                
                System.out.println("Received: "+lineIn);
                escritor.flush();
                if(lineIn.equals("FIN")){
                    Servidor3.NoClients--;
			      break;
                }else{
               escritor.println("Echo... "+lineIn);
               escritor.flush();
            }
             }
             
                     
            }
             
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
    
}
